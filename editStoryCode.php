<?php
session_start();


require "conn.php";

if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

if(!isset($_POST['Stitle']) || !isset($_POST['Scontent']) || !isset($_POST['Slink'])){
	header('Location: index.php');	
}


$title=$_POST['Stitle'];
$content = $_POST['Scontent'];
$link = $_POST['Slink'];
$sid=$_POST["story_id"];

$stmt = $mysqli->prepare("UPDATE stories SET title=?, content=?, link=? WHERE story_id=?");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('sssi', $title, $content, $link, $sid);
$stmt -> execute();
$stmt->close();


header('Location: storyPage.php?id='.$sid);




?>