<?php
session_start();
require "conn.php";
$sid= $_POST['story_id'];
$Ccontent = $_POST['Ccontent'];
$stmt = $mysqli->prepare("insert into comments (content, author, story_id) values (?,?,?)");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('sii', $Ccontent, $_SESSION['userid'],$sid);
$stmt -> execute();
$stmt->close();

header("Location: storyPage.php?id=$sid");
?>