<?php
  session_start();

  unset($_SESSION['title']);
  unset($_SESSION['content']);
  unset($_SESSION['author']);
  unset($_SESSION['time']);
  include_once('header.php');
?>

<!-- CONTENT HERE -->


<form action="addStory.php" method="POST">
    <input type="submit" value="New Story"  /><br>
</form> 


<?php

require "conn.php";

$stmt = $mysqli->prepare("SELECT story_id,title, content, users.username as author, time FROM stories LEFT JOIN users on (stories.user_id=users.user_id) ORDER BY time DESC;");

if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
 
$stmt->execute();
 
$stmt->bind_result($story_id, $title, $content, $author, $time);
 
$_SESSION['title']=$title;
$_SESSION['content']=$content;
$_SESSION['author']=$author;
$_SESSION['time']=$time;

while($stmt->fetch()){
  echo '<div class="post">';
  echo "<div class='title'><a href = 'storyPage.php?id=$story_id'>".htmlentities($title)."</a></div>";
  echo "<div class='author'>&gt; &gt; &gt; By $author  $time</div>";
  echo '<p class="content">'.substr(htmlentities($content),0,600).'</p>'; 
  echo '</div>';
}


 
$stmt->close();
?>




  </div></body>
  </html>





