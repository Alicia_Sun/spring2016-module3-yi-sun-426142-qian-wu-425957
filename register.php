<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>AnAwesomeNewsWebsite</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<style type="text/css">

.login-box{
	width: 400px;
	height: 200px;
	top: 30%;
	margin: 0 auto;
	border: solid 1px #000000;
	/*position: absolute;*/
}

.box-header{
	background-color: #000000;
	color: #ffffff;
	font-size: 20px;
	height: 30px;
	text-align: center;
	font-family: 'Open Sans Condensed', sans-serif;
}

.box-body label{
	margin: 10px;
	display: block;
	font-size: 18px;
}

.box-body {
	text-align: center;
}

.box-body input[type=submit]{
	border: solid 1px #000000;
	font-size: 18px;
	background-color: #ffffff;
	color: #000000;
	margin: 5px;
	cursor: pointer;
}

</style>
</head>
<body>

 
<!-- CONTENT HERE -->

<div class="login-box">
<div class="box-header">Register for AnAwesomeNewsWebsite</div>
<div class="box-body">
<form action="register_code.php" method="POST">
		<label>Username: <input type="text" name="id"></label>
		<label>Password: <input type="password" name="pw"></label>
		<input type="submit" value="Register"  /><br>
</form>	
</div>
</div>
</body>
</html>
