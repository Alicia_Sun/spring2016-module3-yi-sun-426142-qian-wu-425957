<?php
require "conn.php";

$stmt = $mysqli->prepare("DELETE from comments where comment_id = ?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$sid=$_GET['sid'];
$stmt->bind_param('i', $_GET['cid']);

$stmt->execute();

$stmt->close();

header('Location: storyPage.php?id='.$sid);	


?>