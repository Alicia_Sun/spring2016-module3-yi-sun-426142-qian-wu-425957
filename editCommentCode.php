<?php
session_start();


require "conn.php";
$sid=$_POST["story_id"];
if(!isset($_POST['Ccontent'])){
	header('Location: storyPage.php?id='.$sid);	
}

$content = $_POST['Ccontent'];
$cid=$_POST['comment_id'];

$stmt = $mysqli->prepare("UPDATE comments SET content=? WHERE comment_id=?");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('si', $content,$cid);
$stmt -> execute();
$stmt->close();


header('Location: storyPage.php?id='.$sid);




?>