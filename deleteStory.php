<?php
require "conn.php";

$stmt = $mysqli->prepare("DELETE from comments where story_id = ?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}

$stmt->bind_param('i', $_GET['sid']);

$stmt->execute();

$stmt->close();

$stmt = $mysqli->prepare("DELETE from stories where story_id = ?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
 
$stmt->bind_param('i',  $_GET['sid']);

$stmt->execute();

$stmt->close();

header('Location: user_page.php');	


?>