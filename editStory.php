<?php 
session_start();
include_once('header.php'); 
require "conn.php";
$stmt = $mysqli->prepare("SELECT title, content, link FROM stories where stories.story_id = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit();
}
$sid = $_GET['sid'];
$stmt->bind_param('s', $sid);
$stmt->execute();
$stmt->bind_result($title, $content, $link);
$stmt->fetch();
$stmt->close();

?>

<form action="editStoryCode.php" method="POST">
<input type="hidden" name="story_id" value="<?php echo $sid;?>"/>
<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
<h1>Title</h1>
<textarea rows = "1" cols = "70" name= "Stitle" class = "title"><?php echo $title; ?>
</textarea>
<h1>Content</h1>
<textarea rows = "10" cols = "70" name= "Scontent" class = "content"><?php echo $content; ?>
</textarea>
<h1>Link</h1>
<textarea rows = "3" cols = "70" name= "Slink" class = "link"><?php echo $link; ?></textarea>

<p><input type="submit" value="Submit"  /></p>

</form>

</body>
</html>