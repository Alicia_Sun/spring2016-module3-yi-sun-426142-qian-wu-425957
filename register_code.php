<?php

session_start();
/*if(!isset($_POST['id'])||!isset($_POST['pw'])){
    header('Location: index.php');
}*/


require "conn.php";



$id=$_POST['id'];
$pw = $_POST['pw'];
$stmt = $mysqli->prepare("select count(*), username, password from users where username = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	
}
$stmt->bind_param('s', $id);
$stmt -> execute();
$stmt->bind_result($count, $username, $hashpass);

$stmt->fetch();

$stmt->close();
// echo $count;
if($count > 0){
	// echo $count;
	header('Location: user_page.php'); //already registered
	$_SESSION['username']=$id;
	exit();
}
if($count == 0 && isset($_POST['id']) && isset($_POST['pw'])){
	$_SESSION['username']=$id;
	$hashpass = crypt($pw,'$1$1cyVForA$brBHURa21G.veLEF2txKB1');	
	$stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
	if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
	}
	$stmt->bind_param('ss', $id, $hashpass);
	$stmt -> execute();
	$stmt->close();

	header('Location: user_page.php');	
}
else{
	
	header('Location: register.php');	
}

?>