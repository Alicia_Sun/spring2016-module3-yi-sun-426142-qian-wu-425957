<?php include_once('header.php'); 
require "conn.php";
$cate=$_GET["cat"];

$stmt = $mysqli->prepare("SELECT story_id,title, content, users.username as author, time FROM stories LEFT JOIN users on (stories.user_id=users.user_id) WHERE category='$cate' ORDER BY time DESC;");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
 
$stmt->execute();
 
$stmt->bind_result($sid,$title, $content, $author, $time);
 
$_SESSION['title']=$title;
$_SESSION['content']=$content;
$_SESSION['author']=$author;
$_SESSION['time']=$time;

while($stmt->fetch()){
	echo '<div class="post">';
	echo '<div class="title"><a href = "storyPage.php?id='.$sid.'">'.$title.'</a></div>';
	echo "<div class='author'>&gt; &gt; &gt; By $author  $time</div>";
	echo '<p class="content">'.substr($content,0,600).'</p>'; 
	echo '</div>';
}


 
$stmt->close();


?>


</body>
  </html>