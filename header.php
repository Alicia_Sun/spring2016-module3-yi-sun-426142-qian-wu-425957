<?php
  session_start();
?>
<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>NEWS</title>
<link rel="stylesheet" type="text/css" href="main.css" >
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
</head>
<body>

<header>
<!-- <div class="user-info"><?php echo $_SESSION['username']; ?> | <a href="logout.php">Logout</a></div> -->
<?php
	$username = $_SESSION['username'];
	if(isset($_SESSION['username'])){
		echo "<div class='user-info'> $username| <a href='logout.php'>Logout</a></div>";
	}
	else{
		echo "<div class='user-info'> Guest | <a href='logout.php'>Back to main</a></div>";
	}
?>
<ul class="category">
	<li><a href="user_page.php">Home</a></li>
	<li><a href="view_select.php?cat=Pol">Politics</a></li>
	<li><a href="view_select.php?cat=Econ">Economics</a></li>
	<li><a href="view_select.php?cat=Sports">Sports</a></li>
	<li><a href="view_select.php?cat=Ent">Entertainment</a></li>
	<li><a href="view_select.php?cat=Tech">Technology</a></li>	
</ul>
</header>

<br>
<br>
<br>
<hr class="nav-hr">
