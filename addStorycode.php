<?php
session_start();


require "conn.php";

if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

if(!isset($_POST['Stitle']) || !isset($_POST['Scontent']) || !isset($_POST['Slink'])){
	header('Location: index.php');	
}



$title=$_POST['Stitle'];
$content = $_POST['Scontent'];
$link = $_POST['Slink'];
$cate=$_POST['category'];



$stmt = $mysqli->prepare("insert into stories (title, content, user_id, link, category) values (?, ?, ?, ?,?)");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('ssiss', $title, $content, $_SESSION['userid'], $link, $cate);
$stmt -> execute();
$stmt->close();

$stmt = $mysqli->prepare("select story_id from stories order by story_id desc");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->execute();
 
$stmt->bind_result($story_id);

$stmt->fetch();

$stmt->close();

//fill the link first



header('Location: user_page.php');




?>