<?php
session_start();
require "conn.php";
/*if(!isset($_POST['id'])||!isset($_POST['pw'])){
    header('Location: index.php');
}*/

if(!isset($_POST['id']) || !isset($_POST['pw'])){
	header('Location: index.php');	
}


$id=$_POST['id'];
$pw = $_POST['pw'];
$stmt = $mysqli->prepare("SELECT count(*), user_id, username, password from users where username = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	
}
$stmt->bind_param('s', $id);
$stmt -> execute();
$stmt->bind_result($count, $userid,$username, $hashpass);

$stmt->fetch();

$stmt->close();

if($count> 0 && crypt($pw,'$1$1cyVForA$brBHURa21G.veLEF2txKB1') == $hashpass ){//login success
	$_SESSION['username']=$id;
	$_SESSION['userid']=$userid;
	$_SESSION['token'] = substr(md5(rand()), 0, 10); 
	header('Location: user_page.php');	
}

if($count> 0 && crypt($pw,'$1$1cyVForA$brBHURa21G.veLEF2txKB1') != $hashpass ){//login fail
	
	header('Location: index.php');	

}

if($count == 0 && isset($_POST['id']) && isset($_POST['pw'])){
	echo 'Please register.' ;//no user exists
}









?>